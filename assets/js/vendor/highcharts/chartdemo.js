'use strict';
window.chartDemoLoaded = true;
(function () {
    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });

    new Highcharts.Chart({
        chart: {
            renderTo: document.getElementById('graph'),
            type: 'area',
            animation: Highcharts.svg,
            margin: [0, 30, 25, 0],
            events: {
                load: function () {
                    var series = this.series[0];
                    setInterval(function () {
                        var x = (new Date()).getTime(),
                            y = 1 + Math.random();
                        series.addPoint([x, y], true, true);
                    }, 1000);
                }
            }
        },
        title: {text: ''},
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150
        },
        yAxis: {
            //title: {text: 'Value'},
            opposite: true,
            labels: {
                //align: 'right',
                x: 5,
                y: 0
            },
            plotLines: [{
                color: '#1D70A5',
                //dashStyle: 'longdashdot',
                value: 1.7,
                width: 2,
                label: {
                    text: '1.7',
                    align: 'right',
                    x: 20,
                    y: 2,
                    useHTML: true,
                    style: {
                        backgroundColor: '#1D70A5',
                        color: '#fff',
                        fontSize: '8px',
                        fontWeight: 'bold',
                        //display: 'block',
                        //width: '20px',
                        //height: '10px',
                        padding:'3px 5px'
                    }

                }
            }]
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' + Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' + Highcharts.numberFormat(this.y, 2);
            }
        },
        legend: {
            enabled: false
        },
        exporting: {
            enabled: false
        },
        series: [
            {
                name: 'Random data',
                data: (function () {
                    var data = [], time = (new Date()).getTime(), i;
                    for (i = -19; i <= 0; i += 1) {
                        data.push({
                            x: time + i * 1000,
                            y: Math.random()
                        });
                    }
                    return data;
                }())
            }
        ]
    });
})();