'use strict';

import Actions from '../actions/actions';

class ChartStore {
	constructor() {
		this.channels = {};
		this.bindAction(Actions.initApp, ChartStore.addChannels);
		this.bindAction(Actions.updateTicker, this.updateChart);
		this.bindAction(Actions.getChart, this.getChart);
	}

	static addChannels(mainsections) {
		mainsections.map(m => {
			return (this.channels[m.symbol] = {
				newData: [],
				chartData: [],
				selectedOrderId: null,
				selectedOrderOpenPrice: null,
				earlyExitPayout: 0
			});
		});
		return false;
	}

	updateChart(quoteData) {
		var symbol = quoteData.symbol,
			chan = this.channels[symbol];
		if (!symbol || !chan) return false;
		var x = quoteData.time,
			y = quoteData.bid,
			r = quoteData.change;
		chan.newData = {x, y, r};
	}

	getChart(symbolchartDataObj) {
		var symbol = symbolchartDataObj.symbol,
			chan = this.channels[symbol];
		if (!symbol || !chan) return false;
		if (!symbolchartDataObj.chartData.length) return false;
		chan.chartData = symbolchartDataObj.chartData;
	}
}

export default alt.createStore(ChartStore, 'ChartStore');
