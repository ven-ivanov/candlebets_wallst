'use strict';

import Actions from '../actions/actions';
import isEqual from '../utils/isequal';

class IndicatorStore {
	constructor() {
		this.channels = {};
		this.bindAction(Actions.initApp, IndicatorStore.addChannels);
		this.bindAction(Actions.setIndicator, this.setIndicator);
	}

	static addChannels(mainsections) {
		mainsections.map(m => {
			return (this.channels[m.symbol] = {
				indicatorSettings: {},
				chartIndicators: []
			});
		});
		return false;
	}

	setIndicator(paramsObj) {
		var symbol = paramsObj.symbol,
			chan = this.channels[symbol];
		if (!symbol || !chan) return false;
		var indicator = paramsObj.indicator,
			base = paramsObj.base,
			period = paramsObj.period,
			type = paramsObj.type,
			newIndicator = {indicator, base, period, type};
		if (indicator === undefined) {
			chan.chartIndicators = [];
			return;
		}
		for (var i = 0, z = chan.chartIndicators.length; i < z; i++) {
			if (isEqual(newIndicator, chan.chartIndicators[i])) return false;
		}
		if (indicator === 'MA') {
			chan.chartIndicators = (chan.chartIndicators.filter(function (v) {
				return v.indicator === 'MA';
			}));
			if (chan.chartIndicators.length >= 2) chan.chartIndicators.pop();
			chan.chartIndicators.unshift(newIndicator);
		} else {
			chan.chartIndicators = [newIndicator];
		}
		chan.indicatorSettings = paramsObj;
	}
}

export default alt.createStore(IndicatorStore, 'IndicatorStore');
