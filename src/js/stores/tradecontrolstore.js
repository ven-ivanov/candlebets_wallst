'use strict';

import Actions from '../actions/actions';
import InitConfig from '../utils/initconfig';

class TradeOrderControlStore {
	constructor() {
		this.channels = {};
		this.bindAction(Actions.initApp, this.addChannels);
		this.bindAction(Actions.setExpiryValue, this.setExpiryValue);
		this.bindAction(Actions.setAmountValue, this.setAmountValue);
	}

	addChannels(mainsections) {
		mainsections.map(m => {
			return (this.channels[m.symbol] = {
				roi: InitConfig.ROI,
				amount: InitConfig.INIT_AMOUNT,
				selectedAmount: InitConfig.INIT_AMOUNT,
				expiry: InitConfig.INIT_EXPIRY,
				selectedExpiry: InitConfig.INIT_EXPIRY,
				selectedOrderId: null,
				selectedOrderExpiry: null,
				orderType: null,
				earlyExitPayout: 0
			});
		});
		return false;
	}

	resetCountdown(channel) {
		var chan = channel;
		chan.amount = chan.selectedAmount;
		chan.expiry = chan.selectedExpiry;
		chan.earlyExitPayout = 0;
	}

	setExpiryValue(valSymObj) {
		var symbol = valSymObj.symbol,
			chan = this.channels[symbol];
		if (!symbol || !chan) return false;
		chan.selectedExpiry = Number(valSymObj.value);
		this.resetCountdown(chan);
	}

	setAmountValue(valSymObj) {
		var symbol = valSymObj.symbol,
			chan = this.channels[symbol];
		if (!symbol || !chan) return false;
		chan.selectedAmount = Number(valSymObj.value);
		this.resetCountdown(chan);
	}
}

export default alt.createStore(TradeOrderControlStore, 'TradeOrderControlStore');
