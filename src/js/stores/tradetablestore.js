'use strict';

import Actions from '../actions/actions';
import InitConfig from '../utils/initconfig';
import fmtMoney from '../utils/fmtmoney';
import earlyExitCalc from '../utils/earlyexitcalc';

class TradeTableStore {
	constructor() {
		this.channels = {};
		this.userIn = false;
		this.bindAction(Actions.initApp, TradeTableStore.addChannels);
		this.bindAction(Actions.setTradeTableView, this.setTradeTableView);
		this.bindAction(Actions.openTrade, this.openTrade);
		this.bindAction(Actions.closeTrade, this.closeTrade);
		this.bindAction(Actions.updateTicker, this.updateOpenTrades);
		this.bindAction(Actions.setSelectedTrade, this.setSelectedTrade);
		this.bindAction(Actions.setExpiryValue, this.unsetSelectedTrade);
		this.bindAction(Actions.setAmountValue, this.unsetSelectedTrade);
		this.bindAction(Actions.userLoggedIn, this.userLoggedIn);
		this.bindAction(Actions.getUserOpenTrades, this.getUserTrades);
		this.bindAction(Actions.getUserClosedTrades, this.getUserTrades);
	}

	static addChannels(mainsections) {
		mainsections.map(m => {
			return (this.channels[m.symbol] = {
				alias: m.alias,
				shift: m.shift,
				activeView: 'opentrades',
				openTrades: [],
				closeTrades: [],
				selectedTradeIndex: null,
				tradeResult: null
			});
		});
		return false;
	}

	setTradeTableView(valSymObj) {
		var symbol = valSymObj.symbol,
			chan = this.channels[symbol];
		if (!symbol || !chan) return false;
		chan.activeView = valSymObj.value;
	}

	openTrade(tradeData) {
		if (!this.userIn) return false;
		var symbol = tradeData.symbol,
			chan = this.channels[symbol];
		if (!symbol || !chan) return false;
		var shift = chan.shift;
		chan.selectedTradeIndex = (chan.openTrades.push({
				'ticket': tradeData.order,
				'symbol': symbol,
				'bet amount': fmtMoney(tradeData.amount),
				'payout': fmtMoney(tradeData.amount * tradeData.payout),
				'roi': InitConfig.ROI + '%',
				'type': (tradeData.flags === InitConfig.CALL_FLAG) ? 'CALL' : 'PUT',
				'open': this.roundToShift(tradeData.price_open, shift),
				'expiry': tradeData.expiration_in,
				'profit/loss': '',
				'time': tradeData.expiration_in,
				'current': 0,
				'expiration': tradeData.expiration,
				'flags': tradeData.flags,
				'open time': tradeData.time_open
			})) - 1;
		chan.tradeResult = null;
		chan.activeView = 'opentrades';
	}

	closeTrade(tradeData) {
		if (!this.userIn) return false;
		var symbol = tradeData.symbol,
			chan = this.channels[symbol];
		if (!symbol || !chan) return false;
		var tradeResult = tradeData.profit,
			openTrades = chan.openTrades,
			closeTrades = chan.closeTrades,
			currTrade,
			currTradeResult,
			eeTime;
		for (var i = 0; i < openTrades.length; i++) {
			currTrade = openTrades[i];
			if (currTrade.ticket === tradeData.order) {
				currTradeResult = tradeResult - tradeData.amount;
				eeTime = this.getEETime(tradeData.expiration_in - ((tradeData.time_close - tradeData.time_open) / 1000));

				closeTrades.push({
					'ticket': tradeData.order,
					'symbol': symbol,
					'bet amount': currTrade['bet amount'],
					'payout': currTrade.payout,
					'roi': currTrade.roi,
					'type': currTrade.type,
					'open': tradeData.price_open,
					'close': tradeData.price_close,
					'expiry': currTrade.expiry,
					'open time': tradeData.time_open,
					'close time': tradeData.time_close,
					'EE': eeTime,
					'profit/loss': fmtMoney(currTradeResult)
				});
				openTrades.splice(i, 1);
				window.setTimeout(() => {
					Actions.updateBalance(currTradeResult + Number(currTrade['bet amount'].replace(/[^0-9\.-]+/g, '')));
				});
				if (!openTrades.length) {
					chan.selectedTradeIndex = null;
				} else {
					if (chan.selectedTradeIndex > (openTrades.length - 1)) chan.selectedTradeIndex = openTrades.length - 1;
				}
				if (chan.tradeResult === null) {
					chan.tradeResult = currTradeResult;
					window.setTimeout(() => {
						chan.tradeResult = null;
						this.emitChange();
					}, 3000);
				}
				break;
			}
		}
	}

	updateOpenTrades(quoteData) {
		if (!this.userIn) return false;
		var symbol = quoteData.symbol,
			chan = this.channels[symbol];
		if (!symbol || !chan || !chan.openTrades.length) return false;
		var openTrades = chan.openTrades;
		var currTrade,
			shift = chan.shift,
			bid = this.roundToShift(quoteData.bid, shift),
			timestamp = quoteData.time,
			amount,
			currentCandleValue,
			earlyExitPayout,
			currRound,
			tradeRounds;
		for (var i = 0; i < openTrades.length; i++) {
			currTrade = openTrades[i];
			currTrade.tradeSelected = (i === chan.selectedTradeIndex);
			currRound = Math.floor((timestamp - currTrade['open time']) / 1000) + 1;
			if (currRound <= 0 || currRound >= currTrade.expiry) continue;
			tradeRounds = currTrade.expiry;
			currTrade.time = tradeRounds - currRound;
			amount = Number(currTrade['bet amount'].replace(/[^0-9\.-]+/g, ''));
			currTrade.current = bid;
			currentCandleValue = Math.round((bid - Number(currTrade.open)) / Math.pow(0.1, Number(shift)));
			earlyExitPayout = ((earlyExitCalc(currentCandleValue, currTrade.flags === InitConfig.CALL_FLAG, tradeRounds, currRound) * amount) - amount);
			if (Math.abs(earlyExitPayout) > amount) earlyExitPayout = amount * (earlyExitPayout < 0 ? -1 : 1);
			currTrade['profit/loss'] = fmtMoney(earlyExitPayout);
		}
	}

	setSelectedTrade(tradeData) {
		if (!this.userIn) return false;
		var symbol = tradeData.symbol,
			chan = this.channels[symbol];
		if (!symbol || !chan) return false;
		var openTrades = chan.openTrades,
			currTrade;
		//if (openTrades.length < 2) return false;
		for (var i = 0; i < openTrades.length; i++) {
			currTrade = openTrades[i];
			if (tradeData.tradeId === currTrade.ticket) {
				chan.selectedTradeIndex = i;
				break;
			}
		}
	}

	unsetSelectedTrade(valSymObj) {
		var symbol = valSymObj.symbol,
			chan = this.channels[symbol];
		if (!symbol || !chan) return false;
		chan.selectedTradeIndex = null;
	}

	userLoggedIn(userData) {
		this.userIn = userData ? true : false;
		for (var symbol in this.channels) {
			var chan = this.channels[symbol];
			chan.activeView = 'opentrades';
			chan.openTrades = [];
			chan.closeTrades = [];
			chan.selectedTradeIndex = null;
		}
	}

	getUserTrades(tradesData) {
		if (!this.userIn) return false;
		var trades = tradesData,
			symbol,
			chan,
			currTrade,
			ticket,
			betAmount,
			payout,
			roi = InitConfig.ROI + '%',
			type,
			priceOpen,
			priceClose,
			expiry,
			timeOpen,
			timeClose,
			eeTime;
		if (!trades.length) {
			Actions.setLoading(false);
			return false;
		}
		for (var i = 0, z = trades.length; i < z; i++) {
			currTrade = trades[i];
			symbol = currTrade.symbol;
			chan = this.channels[symbol];
			ticket = currTrade.order;
			betAmount = fmtMoney(currTrade.amount);
			payout = fmtMoney(currTrade.amount * currTrade.payout);
			type = (currTrade.flags === InitConfig.CALL_FLAG) ? 'CALL' : 'PUT';
			priceOpen = currTrade.price_open;
			priceClose = currTrade.price_close;
			expiry = currTrade.expiration_in;
			timeOpen = currTrade.time_open;
			timeClose = currTrade.time_close;
			if (timeClose) {
				// closed trade
				eeTime = this.getEETime(expiry - ((timeClose - timeOpen) / 1000));
				chan.closeTrades.push({
					'ticket': ticket,
					'symbol': symbol,
					'bet amount': betAmount,
					'payout': payout,
					'roi': roi,
					'type': type,
					'open': priceOpen,
					'close': priceClose,
					'expiry': expiry,
					'open time': timeOpen,
					'close time': timeClose,
					'EE': eeTime,
					'profit/loss': fmtMoney(currTrade.profit || currTrade.amount * -1)
				});
			} else {
				chan.selectedTradeIndex = (chan.openTrades.push({
						'ticket': ticket,
						'symbol': symbol,
						'bet amount': betAmount,
						'payout': payout,
						'roi': roi,
						'type': type,
						'open': priceOpen,
						'expiry': expiry,
						'profit/loss': '',
						'time': expiry,
						'current': 0,
						'expiration': currTrade.expiration,
						'flags': currTrade.flags,
						'open time': timeOpen
					})) - 1;
			}
		}
		Actions.setLoading(false);
	}

	roundToShift(val, shift) {
		if (!isNaN(val)) {
			var mult = Math.pow(10, shift);
			return (Math.round(val * mult) / mult);
		} else {
			return val;
		}
	}

	getEETime(tradeTime) {
		switch (true) {
			case tradeTime < 0:
				return '';
			case tradeTime < 1:
				return 1;
			default:
				return Math.floor(tradeTime);
		}
	}

}

export default alt.createStore(TradeTableStore, 'TradeTableStore');
