'use strict';

import Actions from '../actions/actions';
const MAX_TICKER_LENGTH = 22;

class TickerStore {
	constructor(){
		this.serverTs = new Date().getTime();
		this.channels = {};
		this.bindAction(Actions.initApp, TickerStore.addChannels);
		this.bindAction(Actions.initTicker, this.initTicker);
		this.bindAction(Actions.updateTicker, this.updateTicker);
	}

	static addChannels(mainsections){
		mainsections.map(m =>{
			return (this.channels[m.symbol] = {
				tickerQueue: []
			});
		});
		return false;
	}

	initTicker(symDataObj){
		var symbol = symDataObj.symbol,
			chan = this.channels[symbol];
		if (!symbol || !chan) return false;
		var tickHistory = symDataObj.data.slice(-MAX_TICKER_LENGTH);
		if (!tickHistory.length) return false;
		tickHistory = tickHistory.reverse();
		var currResult;
		chan.tickerQueue = tickHistory.map(i =>{
			currResult = i[5];
			return currResult *= this.getResultSign(symbol, currResult);
		});
	}

	updateTicker(quoteData){
		var symbol = quoteData.symbol,
			chan = this.channels[symbol];
		if (!symbol || !chan) return false;
		this.serverTs = quoteData.time;
		var tickerQueue = chan.tickerQueue,
			result = quoteData.change;
		result *= this.getResultSign(symbol, result);
		if (tickerQueue.length >= MAX_TICKER_LENGTH) tickerQueue.pop();
		if (result !== undefined) tickerQueue.unshift(result);
		chan.tickerQueue = tickerQueue;
	}

	// formatter helpers
	//noinspection OverlyComplexFunctionJS
	getResultSign(symbol, n){
		switch (symbol){
			case 'REDBLACK':
				if (n === 0){
					return 0;
				} else if (n < 11){
					return n & 1 ? -1 : 1;
				} else if (n < 19){
					return n & 1 ? 1 : -1;
				} else if (n < 29){
					return n & 1 ? -1 : 1;
				} else{
					return n & 1 ? 1 : -1;
				}
				break;
			case 'ODDEVEN':
				if (n === 0){
					return 0;
				} else if (n & 1){
					return -1;
				} else{
					return 1;
				}
				break;
			case 'HIGHLOW':
				if (n === 0){
					return 0;
				} else if (n > 18){
					return 1;
				} else{
					return -1;
				}
				break;
		}
		return false;
	}
}

export default alt.createStore(TickerStore, 'TickerStore');
