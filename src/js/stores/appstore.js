'use strict';

import Actions from '../actions/actions';

class AppStore {
	constructor() {
		this.inited = false;
		this.mainsections = [];
		this.isModalOpen = false;
		this.bindAction(Actions.initApp, this.initApp);
		this.bindAction(Actions.openModal, this.openModal);
		this.bindAction(Actions.closeModal, this.closeModal);
	}

	initApp(sections) {
		if (this.inited) return false;
		this.setState({
			mainsections: sections,
			isModalOpen: false,
			modalSymbol: null,
			online: window.navigator.onLine,
			inited: true
		});
	}

	openModal(symbol) {
		this.setState({isModalOpen: true, modalSymbol: symbol});
	}

	closeModal() {
		this.setState({isModalOpen: false});
	}

}

export default alt.createStore(AppStore, 'AppStore');
