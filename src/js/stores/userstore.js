'use strict';

import Actions from '../actions/actions';

class UserStore {
	constructor() {
		this.username = null;
		this.userbalance = 0;
		this.bindAction(Actions.userLoggedIn, this.userLoggedIn);
		this.bindAction(Actions.openTrade, this.openTrade);
		this.bindAction(Actions.updateBalance, this.updateBalance);
		this.bindAction(Actions.updateBalance, this.updateBalance);
	}

	userLoggedIn(userData) {
		if (userData && userData.username && userData.userbalance) {
			var userBalance = parseInt(userData.userbalance) ? userData.userbalance : 0;
			this.setState({
				username: userData.username,
				userbalance: userBalance
			});
		} else {
			this.setState({
				username: null,
				userbalance: 0
			});
		}
	}

	openTrade(tradeData) {
		var amount = parseFloat(tradeData.amount),
			newBalance = this.userbalance -= amount;
		this.setState({userbalance: newBalance});
	}

	updateBalance(profitLoss) {
		var tradeResult = parseFloat(profitLoss),
			newBalance = this.userbalance += tradeResult;
		this.setState({userbalance: newBalance});
		localStorage.setItem('uzrb', newBalance);
	}
}

export default alt.createStore(UserStore, 'UserStore', false);
