'use strict';

import Actions from '../actions/actions';
import chartPanelConfig from '../utils/chartPanelConfig';

class ChartPanelStore {
	constructor() {
		this.channels = {};
		this.bindAction(Actions.initApp, ChartPanelStore.addChannels);
		this.bindAction(Actions.setChartSettings, this.setChartSettings);
	}

	static addChannels(mainsections) {
		mainsections.map(m => {
			return (this.channels[m.symbol] = {
				chartres: 'Tick',
				charttype: 'candlestick'
			});
		});
		return false;
	}

	setChartSettings(settingObj) {
		var symbol = settingObj.symbol,
			chan = this.channels[symbol];
		if (!symbol || !chan) return false;
		var chanHasTimeframe = false;
		if (settingObj.charttype && settingObj.charttype !== chan.charttype) {
			var chartResRadioButtons = chartPanelConfig.radioGroupsConfig[0].buttons[settingObj.charttype];
			for (var i = 0, l = chartResRadioButtons.length; i < l; i++) {
				if (chartResRadioButtons[i].value === chan.chartres) {
					chanHasTimeframe = true;
					break;
				}
			}
			if (!chanHasTimeframe) chan.chartres = 'Tick';
			chan.charttype = settingObj.charttype;
		}

		if (settingObj.chartres && settingObj.chartres !== chan.chartres) chan.chartres = settingObj.chartres;
	}
}

export default alt.createStore(ChartPanelStore, 'ChartPanelStore');
