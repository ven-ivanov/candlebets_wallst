'use strict';

import Store from '../stores/appstore';
import Actions from '../actions/actions';
import UserPanel from '../components/userpanel.jsx';
import MainSection from '../components/mainsection.jsx';
import Modal from '../components/modal.jsx';
import IndicatorsForm from '../components/indicatorsform.jsx';

export default class App extends React.Component {
	static displayName = 'App';

	constructor(props) {
		super(props);
		this.state = Store.getState();
	}

	componentDidMount() {
		Store.listen(this.onStateChange);
		window.addEventListener('online', this.checkIfOnline);
		window.addEventListener('offline', this.checkIfOnline);
	}

	componentWillUnmount() {
		Store.unlisten(this.onStateChange);
		window.removeEventListener('online', this.checkIfOnline);
		window.removeEventListener('offline', this.checkIfOnline);
	}

	render() {
		var mainSectionsContent = this.state.mainsections.map((m, i)=> {
            if( i === 0){
                return (
                    <MainSection alias={m.alias} shift={m.shift} symbol={m.symbol} online={this.state.online} key={'mainsec' + i} tab={'mainsec' + i} active='active'/>
                );
           }else {
                return (
                    <MainSection alias={m.alias} shift={m.shift} symbol={m.symbol} online={this.state.online} key={'mainsec' + i} tab={'mainsec' + i}/>
                );
            }
		});

		return (
			<div>
                {/*<UserPanel />*/}
				{mainSectionsContent}
				<Modal transitionName="modal-anim" width="400px" height="200px">
					<h3>Indicator options </h3>
					<IndicatorsForm symbol={this.state.modalSymbol}/>
				</Modal>
			</div>
		);
	}

	checkIfOnline = () => {
		var symbols = this.state.mainsections, i, z;
		if (!window.navigator.onLine) {
			for (i = 0, z = symbols.length; i < z; i++) {
				Actions.subscribeToQuoteFeed(symbols[i].symbol, 'unsubscribe');
			}
			this.setState({online: false});
		} else {
			for (i = 0, z = symbols.length; i < z; i++) {
				Actions.subscribeToQuoteFeed(symbols[i].symbol);
			}
			this.setState({online: true});
		}
	};

	onStateChange = () => {
		this.setState(Store.getState());
	};
}
