'use strict';

import Store from '../stores/chartstore';
import SelectedTradeStore from '../stores/tradetablestore';
import IndicatorStore from '../stores/indicatorstore';
import Actions from '../actions/actions';
import chartTheme from '../utils/highcharts_theme';
import gimmeNum from '../utils/gimmenum';
import isEqual from '../utils/isequal';
import Indicators from '../utils/indicators';

const CHART_PERIODS = {
	Tick: 1000,
	S30: 30000,
	M1: 60000,
	M2: 120000,
	M4: 240000,
	M5: 300000,
	M8: 480000,
	M12: 720000,
	M15: 900000,
	M30: 1800000,
	H1: 3600000,
	H2: 7200000,
	H4: 14400000,
	H8: 28800000,
	H12: 43200000,
	D1: 86400000
};

const CHART_DATAGROUPS = {
	S30: [
		['second', [30]]
	],
	M1: [
		['minute', [1]]
	],
	M2: [
		['minute', [2]]
	],
	M4: [
		['minute', [4]]
	],
	M5: [
		['minute', [5]]
	],
	M8: [
		['minute', [8]]
	],
	M12: [
		['minute', [12]]
	],
	M15: [
		['minute', [15]]
	],
	M30: [
		['minute', [30]]
	],
	H1: [
		['hour', [1]]
	],
	H2: [
		['hour', [2]]
	],
	H4: [
		['hour', [4]]
	],
	H8: [
		['hour', [8]]
	],
	H12: [
		['hour', [12]]
	],
	D1: [
		['day', [1]]
	]
};

// array index - roulette result, array element: [top wick, bottom wick]
const TICK_CANDLES_WICKS = [
	[.4, .4],
	[.6, .3],
	[.2, .6],
	[.6, .4],
	[.4, .5],
	[.4, .4],
	[.3, .7],
	[.6, .3],
	[.4, .5],
	[.6, .3],
	[.5, .4],
	[.3, .6],
	[.7, .4],
	[.3, .7],
	[.6, .3],
	[.5, .4],
	[.5, .3],
	[.2, .6],
	[.6, .3],
	[.3, .6],
	[.7, .2],
	[.2, .7],
	[.7, .3],
	[.3, .6],
	[.5, .3],
	[.2, .7],
	[.6, .4],
	[.3, .8],
	[.8, .2],
	[.8, .3],
	[.2, .7],
	[.8, .2],
	[.4, .5],
	[.7, .2],
	[.2, .8],
	[.7, .3],
	[.2, .8]
];

const PLOT_LINE_CONFIG = chartTheme.yAxis.plotLines[0];

const PLOT_LINE_LBL_BGS = {
	blue: 'iVBORw0KGgoAAAANSUhEUgAAADcAAAAPCAMAAAB3LKe0AAAAXVBMVEUdcKX///8dcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKUdcKVBw+PAAAAAHnRSTlMAAAQIEBQcSU9faHKEiouUqrC/y+Tm6e3x9PX2+PrQEEt8AAAAX0lEQVQoz53NRw6AMAADwZjeewvF/38m4ghCgnjvozXmCo8yfmVeXNjNisutpbvzm410d+m8UHD1Qbq7qJv411Hr9kv6UXLwym1VHBC0VnJAsWsO6ao5xOMgOXjVpzsBZZdEhOe9jLYAAAAASUVORK5CYII=',
	red: 'iVBORw0KGgoAAAANSUhEUgAAADcAAAAPCAMAAAB3LKe0AAAAXVBMVEX+AAD/AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAD+AAAHBwesAAAAHnRSTlMAAAQIEBQcSU9faHKEiouUqrC/y+Tm6e3x9PX2+PrQEEt8AAAAX0lEQVQoz53NRw6AMAADwZjeewvF/38m4ghCgnjvozXmCo8yfmVeXNjNisutpbvzm410d+m8UHD1Qbq7qJv411Hr9kv6UXLwym1VHBC0VnJAsWsO6ao5xOMgOXjVpzsBZZdEhOe9jLYAAAAASUVORK5CYII=',
	green: 'iVBORw0KGgoAAAANSUhEUgAAADcAAAAPCAMAAAB3LKe0AAAAWlBMVEUArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQAArQDPXvnSAAAAHXRSTlMABAgQFBxJT19ocoSKi5SqsL/L5Obp7fH09fb4+lCsWawAAABcSURBVCjPnc1HDoAwAANB00Mn1CTg/3+TMxISxHsfLfBWza/eVG53xTXOMd6lYyDjndkPCm64yHhX2I1/HbUev2peJYekC15xQDY5yQHtqTkYrzmU6yI5JP2nuwGVdkGREsGMkQAAAABJRU5ErkJggg==',
	white: 'iVBORw0KGgoAAAANSUhEUgAAADcAAAAPCAYAAABA8leGAAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOwgAADsIBFShKgAAAABh0RVh0U29mdHdhcmUAcGFpbnQubmV0IDQuMC41ZYUyZQAAALNJREFUSEvd1S0KQlEURWGDwWgwOiGTQ7CaHYNJq6MwOQMH4DREMBgEFbEc1wGDyAbvD3IPLvj6hnc5r2NmvzBF89SwGgNscULz1MBSM1xeQqRG5uphjRtCpcbmmMCf4BnhUoNTrfBA2NTob4bwo3FE6Hzs3/b5VVKMscMBoVPjU3SxgF/IK0KmhufoY4Mw/7b31OASc9wRKjW0lP/zQj1RNbLGCH5o9mieGljLj80SjTN7Ao0j5iSCV7WeAAAAAElFTkSuQmCC'
};

Highcharts.setOptions(JSON.parse(JSON.stringify(chartTheme)));
Highcharts.setOptions({global: {useUTC: false}});

export default class Chart extends React.Component {
	static displayName = 'Chart';

	static propTypes = {
		alias: React.PropTypes.string,
		chartres: React.PropTypes.string.isRequired,
		charttype: React.PropTypes.string.isRequired,
		shift: React.PropTypes.number,
		symbol: React.PropTypes.string.isRequired
	};

	static defaultProps = {
		chartres: 'Tick',
		charttype: 'candlestick'
	};

	constructor(props) {
		super(props);
		this.state = {
			newData: [],
			chartData: [],
			selectedOrderId: null,
			selectedOrderOpenPrice: null,
			earlyExitPayout: 0,
			indicatorSettings: {},
			chartIndicators: []
		};
		if (!this.props.symbol) throw new Error('Symbol undefined');
		this.symbol = this.props.symbol;
		this.inited = false;
		this.prevUpdateTime = 0;
		this.prevBid = 0;
		this.chartDataLoaded = false;
		this.prevlpsig = 0;
		this.errs = 0;
		this.chartres = this.props.chartres;
		this.charttype = this.props.charttype;
		this.chartIsRedrawing = false;
		this.reset = false;
		this.reloadInt = 0;
	}

	componentDidMount() {
		Store.listen(this.onData);
		SelectedTradeStore.listen(this.updateTrade);
		IndicatorStore.listen(this.setIndicator);
	}

	componentWillUnmount() {
		Store.unlisten(this.onData);
		SelectedTradeStore.unlisten(this.updateTrade);
		IndicatorStore.unlisten(this.setIndicator);
	}

	componentWillReceiveProps(nextProps) {
		if (this.props.chartres !== nextProps.chartres || this.props.charttype !== nextProps.charttype) {
			this.chartIsRedrawing = true;
			this.renderChart(nextProps.charttype, nextProps.chartres);
		}
	}

	render() {
		return (
			<div ref="chartNode" className="mainChart"></div>
		);
	}

	onData = () => {
		if (!this.inited) {
			this.renderChart(this.charttype, this.chartres);
			this.inited = true;
			return;
		}
		this.setState(Store.getState().channels[this.symbol], ()=> {
			if (this.chartIsRedrawing) return;
			this.updateChart();
		});
	};

	updateTrade = ()=> {
		var channel = SelectedTradeStore.getState().channels[this.symbol],
			selectedTradeIndex = channel.selectedTradeIndex;
		if (selectedTradeIndex !== null) {
			var selectedOrder = channel.openTrades[selectedTradeIndex];
			this.setState({
					selectedOrderId: selectedOrder.ticket,
					selectedOrderOpenPrice: Number(selectedOrder.open),
					earlyExitPayout: gimmeNum(selectedOrder['profit/loss'])
				},
				() => {
					this.redrawOrderPlotline();
					this.redrawPlotline();
				});
			this.reset = true;
			return;
		}

		if (this.reset) {
			this.setState({
					selectedOrderId: null,
					selectedOrderOpenPrice: null,
					earlyExitPayout: 0
				},
				() => {
					this.redrawOrderPlotline();
				});
			this.reset = false;
		}
	};

	//noinspection FunctionWithMoreThanThreeNegationsJS,OverlyComplexFunctionJS
	updateChart = () => {
		var x = this.state.newData.x;
		if (!x || x === this.prevUpdateTime) return;

		var chartData = this.state.chartData,
			chartDataLength = chartData.length;

		if (!chartDataLength) return;

		var chartIsSpline = (this.charttype === 'areaspline'),
			chartIsTick = (this.chartres === 'Tick'),
			y = this.state.newData.y,
			lastStockDataPoint = chartData[chartDataLength - 1],
			chartPeriods = CHART_PERIODS[this.chartres],
			xCrop = Math.ceil(x / chartPeriods) * chartPeriods;
		if (!this.chartDataLoaded) {
			// load once history data
			try {
				var lpsig = lastStockDataPoint.reduce((pv, cv) => {
					return pv + cv;
				}, 0);
			} catch (e) {
				return;
			}
			if (!lpsig || (lpsig === this.prevlpsig)) {
				this.errs++;
				if (this.errs === 10) {
					this.renderChart(this.charttype, this.chartres);
				}
				return;
			}
			if (!chartIsTick) {
				this.Chart.series[0].setData(chartData);
			} else {
				if (chartIsSpline) {
					var splineData = chartData.map(i => {
						return [i[0], i[4]];
					});
					this.Chart.series[0].setData(splineData);
				} else {
					var tickData = chartData.map(i => {
						return {
							x: i[0],
							open: i[1],
							high: i[2],
							low: i[3],
							close: i[4],
							r: i[5]
						};
					});
					this.Chart.series[0].setData(tickData);
				}
				this.prevBid = lastStockDataPoint[4];
			}
			this.prevlpsig = lpsig;
			this.errs = 0;
			this.chartDataLoaded = true;
			if (!chartIsSpline) this.addIndicator();
			this.Chart.xAxis[0].setExtremes(xCrop - chartPeriods * 60, xCrop, true);
		} else {
			// enable live update
			var r = this.state.newData.r || 0,
				wicks = TICK_CANDLES_WICKS[r],
				topWick = wicks[0],
				bottomWick = wicks[1],
				wickCoeff = 10 * Math.pow(0.1, this.props.shift),
				openPrice = this.prevBid,
				newPoint = {
					x: x,
					open: openPrice,
					high: Math.max(y, openPrice + topWick * wickCoeff),
					low: Math.min(y, openPrice - bottomWick * wickCoeff),
					close: y,
					r: r
				},
				shiftChart = chartIsTick ? true : ((1000 * Math.ceil(x / 1000)) % chartPeriods === 0),
				indicatorIsOn = !!this.Chart.series[1];
			if (chartIsSpline) {
				this.Chart.series[0].addPoint({x, y, r}, false, shiftChart);
			} else {
				this.Chart.series[0].addPoint(newPoint, false, shiftChart, false);
				if (indicatorIsOn && shiftChart && !chartIsTick) {
					// if indicator is on, redraw indicator
					window.setTimeout(() => {
						this.addIndicator();
					}, 2000);
				}
				// if indicator is on, update indicator
				if (indicatorIsOn && chartIsTick) this.updateIndicator();
			}
			if (this.state.selectedOrderId === null) this.redrawPlotline();
			this.Chart.xAxis[0].setExtremes(xCrop - chartPeriods * 60, xCrop, true);
		}
		this.prevUpdateTime = x;
		this.prevBid = y;
	};

	renderChart = (charttype, chartres) => {
		this.chartres = chartres;
		this.charttype = charttype;
		this.chartDataLoaded = false;
		var chartTypeToRender = (charttype === 'areaspline' ? 'Chart' : 'StockChart'),
			shift = this.props.shift,
			dataGrouping = CHART_DATAGROUPS[this.chartres] ? {
				enabled: true,
				forced: true,
				units: CHART_DATAGROUPS[this.chartres]
			} : {enabled: false};
		//noinspection Eslint
		this.Chart = new Highcharts[chartTypeToRender]({
			tooltip: {
				valueDecimals: this.props.shift,
				positioner: function () {
					return {x: 0, y: 0};
				},
				pointFormatter: function () {
					var pointDiff = (this.open - this.close),
						result = typeof this.r === 'undefined' ? '' : this.r + ', ';
					if (isNaN(pointDiff)) {
						return '<span style="color:white; margin: auto 2px"> \u25B6 </span> ' + result + this.y;
					}
					var delimColor = (pointDiff < 0) ? 'green' : 'red';
					var distance = Math.round((this.close - this.open) / Math.pow(0.1, shift));
					if (pointDiff === 0) delimColor = 'white';
					return '<span style="color:' + delimColor + '; margin: auto 2px"> \u25B6 </span>' + result + ' <b>O</b> ' + this.open.toFixed(shift) + ' | <b>H</b> ' + this.high.toFixed(shift) + ' | <b>L</b> ' + this.low.toFixed(shift) + ' | <b>C</b> ' + this.close.toFixed(shift) + ' | <b>D</b> ' + (distance > 0 ? '+' : '') + distance;
				}
			},
			yAxis: {
				labels: {
					x: (charttype === 'areaspline') ? 5 : 35
				}
			},
			rangeSelector: {enabled: false},
			navigator: {enabled: false},
			scrollbar: {enabled: false},
			chart: {
				renderTo: React.findDOMNode(this.refs.chartNode),
				type: charttype
			},
			series: [
				{
					dataGrouping: dataGrouping,
					name: this.props.alias,
					type: charttype
				}
			]
		}, () => {
			this.chartIsRedrawing = false;
			this.prevUpdateTime = 0;
			this.prevBid = 0;
		});
		Actions.getChart(this.symbol, chartres);
		if (this.reloadInt) clearInterval(this.reloadInt);
		this.reloadInt = setInterval(()=> {
			this.renderChart(this.charttype, this.chartres);
		}, 1800000);
	};

	redrawPlotline = () => {
		this.Chart.yAxis[0].removePlotLine('pl1');
		var val = this.state.newData.y;
		if (!val) return;
		var plotLineOptions = PLOT_LINE_CONFIG,
			earlyExitPayout = this.state.earlyExitPayout,
			plotLineLblBg;
		plotLineOptions.id = 'pl1';
		plotLineOptions.value = val;
		plotLineOptions.label.text = val.toFixed(this.props.shift);
		switch (true) {
			case !earlyExitPayout:
				plotLineOptions.color = '#1D70A5';
				plotLineLblBg = PLOT_LINE_LBL_BGS.blue;
				break;
			case (earlyExitPayout > 0):
				plotLineOptions.color = '#00ad00';
				plotLineLblBg = PLOT_LINE_LBL_BGS.green;
				break;
			case (earlyExitPayout < 0):
				plotLineOptions.color = 'red';
				plotLineLblBg = PLOT_LINE_LBL_BGS.red;
				break;
			default:
				plotLineOptions.color = '#1D70A5';
				plotLineLblBg = PLOT_LINE_LBL_BGS.blue;
		}
		plotLineOptions.label.style.background = 'url(data:image/png;base64,' + plotLineLblBg + ') no-repeat -2px -1px';
		this.Chart.yAxis[0].addPlotLine(plotLineOptions);
	};

	redrawOrderPlotline = () => {
		this.Chart.yAxis[0].removePlotLine('pl2');
		if (!this.state.selectedOrderId) return;

		var plotLineOptions = JSON.parse(JSON.stringify(PLOT_LINE_CONFIG)),
			plotLineValue = this.state.selectedOrderOpenPrice;
		plotLineOptions.id = 'pl2';
		plotLineOptions.color = '#fff';
		plotLineOptions.value = plotLineValue;
		plotLineOptions.label.text = plotLineValue.toFixed(this.props.shift);
		plotLineOptions.label.style.color = '#222';
		plotLineOptions.label.style.background = 'url(data:image/png;base64,' + PLOT_LINE_LBL_BGS.white + ') no-repeat -2px -1px';
		this.Chart.yAxis[0].addPlotLine(plotLineOptions);
	};

	setIndicator = () => {
		var nextIndicators = IndicatorStore.getState().channels[this.symbol].chartIndicators,
			thisIndicators = this.state.chartIndicators;
		if (isEqual(nextIndicators[0], thisIndicators[0])) return;
		this.setState({chartIndicators: nextIndicators}, ()=> this.addIndicator());
	};

	//noinspection FunctionWithMultipleLoopsJS
	addIndicator = () => {
		this.clearIndicator();
		var chartIndicators = this.state.chartIndicators,
			currIndicator,
			indicatorData,
			newlinecolor;
		if (!chartIndicators.length) return;
		for (var i = 0, z = chartIndicators.length; i < z; i++) {
			newlinecolor = (i === 0) ? null : '#ff0000';
			currIndicator = chartIndicators[i];
			indicatorData = Indicators.getIndicatorData(currIndicator, this.Chart.series[0], newlinecolor);
			if (indicatorData.length === 1) {
				this.Chart.addSeries(indicatorData[0]);
			} else {
				for (var j = 0, k = indicatorData.length; j < k; j++) {
					this.Chart.addSeries(indicatorData[j]);
				}
			}
		}
	};

	//noinspection FunctionWithMultipleLoopsJS
	updateIndicator = () => {
		var chartIndicators = this.state.chartIndicators,
			currIndicator,
			indicatorData;
		if (!chartIndicators.length) return;
		for (var i = 0, z = chartIndicators.length; i < z; i++) {
			currIndicator = chartIndicators[i];
			indicatorData = Indicators.getIndicatorData(currIndicator, this.Chart.series[0]);
			if (indicatorData.length === 1) {
				this.Chart.series[i + 1].addPoint(indicatorData[0].data[indicatorData[0].data.length - 1], false, true, false);
			} else {
				for (var j = 0, k = indicatorData.length; j < k; j++) {
					var currData = indicatorData[j].data;
					this.Chart.series[j + 1].addPoint(currData[currData.length - 1], false, true, false);
				}
			}
		}
	};

	clearIndicator = () => {
		if (this.Chart.series.length === 1) return;
		while (this.Chart.series.length > 1) this.Chart.series[1].remove(true);
	};
}
