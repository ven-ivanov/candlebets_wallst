'use strict';
// dumb stateless version, requires TradeControl higher order wrapping component

import InitConfig from '../utils/initconfig';
import ServerTime from './servertimedisplay.jsx';

export default class InfoPanel extends React.Component {
	static displayName = 'InfoPanel';

	static propTypes = {
		amount: React.PropTypes.number,
		expiry: React.PropTypes.number,
		roi: React.PropTypes.number,
		selectedAmount: React.PropTypes.number,
		selectedExpiry: React.PropTypes.number,
		selectedOrderId: React.PropTypes.number,
		symbol: React.PropTypes.string.isRequired,
		tradeResult: React.PropTypes.number
	};

	static defaultProps = {
		amount: InitConfig.INIT_AMOUNT,
		selectedAmount: InitConfig.INIT_AMOUNT,
		roi: InitConfig.ROI,
		selectedOrderId: null,
		selectedExpiry: InitConfig.INIT_EXPIRY,
		expiry: InitConfig.INIT_EXPIRY,
		tradeResult: null
	};

	constructor(props) {
		super(props);
		if (!this.props.symbol) throw new Error('Symbol undefined');
		this.reset = false;
	}

	shouldComponentUpdate(nextProps) {
		return (nextProps.expiry !== this.props.expiry || nextProps.selectedAmount !== this.props.selectedAmount || nextProps.tradeResult !== this.props.tradeResult);
	}

	render() {
		var timeleft = this.props.tradeResult ? 0 : this.props.expiry,
			minsleft = Math.floor(timeleft / 60),
			secsleft = timeleft - minsleft * 60,
			payout = this.props.amount + this.props.amount * this.props.roi / 100,
			payoutInt = Math.floor(payout),
			payoutDec = (payout - payoutInt).toFixed(2).replace(/^[0]+/g, '');
		minsleft = (minsleft < 10) ? '0' + minsleft : '' + minsleft;
		secsleft = (secsleft < 10) ? '0' + secsleft : '' + secsleft;
		return (
			<div className="columnParent" style={{justifyContent: 'space-around'}}>
				<div>
					<div className="smalluptext">potential payout:</div>
					<div className="goldie">
						<div className="info">
							<span>&pound;</span>
						</div>
						<h2>{payoutInt}</h2>

						<div className="info">
							<span>{payoutDec}</span>
							<span>gross</span>
						</div>
					</div>
				</div>
				<hr />
				<div>
					<div className="smalluptext">time remaining:</div>
					<div className="goldie">
						<span className="clock"></span>

						<h2 className="clocktxt">{minsleft}:{secsleft}</h2>
					</div>
				</div>
				<hr />
				<div>
					<div className="smalluptext">payout ROI:</div>
					<div className="goldie">
						<h1 className="longjohns">{this.props.roi}%</h1>
					</div>
				</div>
				<hr />
				<ServerTime />
			</div>
		);
	}
}
