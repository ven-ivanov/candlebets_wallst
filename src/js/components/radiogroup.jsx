'use strict';

import Actions from '../actions/actions';

export default class RadioGroup extends React.Component {
	static displayName = 'RadioGroup';

	static propTypes = {
		buttons: React.PropTypes.array.isRequired,
		checkedButton: React.PropTypes.string.isRequired,
		classname: React.PropTypes.string,
		name: React.PropTypes.string.isRequired,
		onChangeAction: React.PropTypes.string.isRequired,
		symbol: React.PropTypes.string.isRequired
	};

	render() {
		var className = 'radiogroup ' + (this.props.classname || ''),
			btnIdPrefix = this.props.symbol + this.props.name + '_rb',
			groupContent = this.props.buttons.map((m, i)=> {
				var currBtnId = btnIdPrefix + i,
					lblClass = (m.value === this.props.checkedButton) ? 'active' : '';
				if (m.classname) lblClass += ' ' + m.classname;
				return (
					<li key={currBtnId}>
						<label className={lblClass} key={currBtnId + 'lbl'} data-value={m.value} onClick={this.setValue}>{m.label}</label>
					</li>
				);
			});

		return (
			<ul className={className}>
				{groupContent}
			</ul>
		);
	}

	addSymbol = (obj) => {
		obj.symbol = this.props.symbol;
		return obj;
	};

	setValue = (e) => {
		var rb = e.currentTarget;
		if (rb.classList.contains('active')) return;
		var settingsObj = {};
		settingsObj[this.props.name] = rb.getAttribute('data-value');
		Actions[this.props.onChangeAction](this.addSymbol(settingsObj));
	};
}
