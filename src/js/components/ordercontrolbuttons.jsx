'use strict';

import Actions from '../actions/actions';
import gimmeNum from '../utils/gimmenum';

export default class TradeTableButtons extends React.Component {
	static displayName = 'TradeTableButtons';
	static propTypes = {
		data: React.PropTypes.string.isRequired,
		rowData: React.PropTypes.object.isRequired
	};

	static defaultProps = {
		data: {},
		rowData: {}
	};

	componentDidMount() {
		if (React.findDOMNode(this.refs.selectedRow)) {
			var parentRow = React.findDOMNode(this.refs.selectedRow).parentNode.parentNode;
			parentRow.className = 'yellowrow';
		}
	}

	render() {
		var eeClass = gimmeNum(this.props.data) > 0 ? 'fw1 up' : 'fw1 down';
		return (<div ref={this.props.rowData.tradeSelected ? 'selectedRow' : ''}>
			<span className={eeClass}>{this.props.data}</span>
			<button onClick={this.requestEarlyExit}>exit</button>
			<button className={this.props.rowData.tradeSelected ? 'active' : ''} onClick={this.handleClick}>view
			</button>
		</div>);
	}

	handleClick = () => {
		Actions.setSelectedTrade({
			symbol: this.props.rowData.symbol,
			tradeId: this.props.rowData.ticket
		});
	};

	requestEarlyExit = () => {
		Actions.requestEarlyExit(this.props.rowData.ticket);
	};
}
