'use strict';

import Store from '../stores/tickerstore';

export default class Ticker extends React.Component {
	static displayName = 'Ticker';

	static propTypes = {
		symbol: React.PropTypes.string.isRequired
	};

	constructor(props) {
		super(props);
		if (!this.props.symbol) throw new Error('Symbol undefined');
		this.symbol = this.props.symbol;
		this.state = {
			tickerQueue: []
		};
	}

	componentDidMount() {
		Store.listen(this.onData);
	}

	componentWillUnmount() {
		Store.unlisten(this.onData);
	}

	render() {
		var tickerContent = this.state.tickerQueue.map((m, i)=> {
			var valueClass = (m > 0) ? 'down' : 'up';
			if (!m) valueClass = '';
			return (
				<div className={valueClass} key={i}>{Math.abs(m)}</div>
			);
		});
		return (
			<div className="panel flexChild ticker">
				<div className="tickerscroller">
					{tickerContent}
				</div>
			</div>);
	}

	onData = () => {
		this.setState(Store.getState().channels[this.symbol]);
	};
}
