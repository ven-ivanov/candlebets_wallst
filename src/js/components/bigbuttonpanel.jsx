'use strict';

import Actions from '../actions/actions';
import InitConfig from '../utils/initconfig';
import classNames from '../utils/classnames';

const CIRCLE_TIMER_CONF = {
	radius: 70,
	width: 10,
	text: null,
	colors: ['transparent', 'transparent'],
	duration: null,
	wrpClass: 'circles-wrp',
	textClass: 'circles-text',
	styleWrapper: true,
	styleText: true
};

export default class BigButtonPanel extends React.Component {
	static displayName = 'BigButtonPanel';

	static propTypes = {
		amount: React.PropTypes.number,
		earlyExitPayout: React.PropTypes.number,
		expiry: React.PropTypes.number,
		online: React.PropTypes.bool,
		orderType: React.PropTypes.any,
		selectedAmount: React.PropTypes.number,
		selectedExpiry: React.PropTypes.number,
		selectedOrderExpiry: React.PropTypes.number,
		selectedOrderId: React.PropTypes.number,
		symbol: React.PropTypes.string.isRequired,
		tradeResult: React.PropTypes.number,
		user: React.PropTypes.object.isRequired
	};

	constructor(props) {
		super(props);
		if (!this.props.symbol) throw new Error('Symbol undefined');
		this.symbol = this.props.symbol;
	}

	componentDidMount() {
		// render initinal radial progress circle
		this.circleTimer = null;
		this.initTimer();
	}

	//noinspection FunctionWithMoreThanThreeNegationsJS
	componentWillReceiveProps(nextProps) {
		if (this.props.tradeResult && !nextProps.tradeResult && !this.props.selectedOrderId) {
			// trade closed state ended
			this.initTimer();
			return;
		}

		if (nextProps.selectedExpiry !== this.props.selectedExpiry) {
			// user clicked on another expiry slider value
			this.initTimer(nextProps.selectedExpiry);
			return;
		}

		if (nextProps.selectedOrderExpiry !== this.props.selectedOrderExpiry) {
			// selected the next open trade in queue
			this.initTimer(nextProps.selectedOrderExpiry);
		}

	}

	//noinspection FunctionWithMoreThanThreeNegationsJS
	render() {
		if (this.props.tradeResult) {
			this.updateTimer(0, this.props.tradeResult);
		} else {
			this.updateTimer(this.props.expiry, this.props.earlyExitPayout);
		}
		var userBalanceOk = parseFloat(this.props.user.userbalance) >= parseFloat(this.props.amount),
			callText = 'PLACE YOUR TRADE',
			eePayout = this.props.earlyExitPayout || null,
			tradeResult = this.props.tradeResult || null,
			bigButtonClass = classNames({
				active: this.props.selectedOrderId && !this.props.tradeResult,
				loss: (!tradeResult && eePayout < 0 || tradeResult && tradeResult < 0)
			}),
			fiveSecsLeft = (this.props.expiry && this.props.expiry <= 5),
			eePayoutInt = (eePayout > 0) ? Math.floor(eePayout) : Math.ceil(eePayout),
			eePayoutDec = Math.abs(eePayout - eePayoutInt).toFixed(2).replace(/^[0]+/g, ''),
			tradeResultInt = (tradeResult > 0) ? Math.floor(tradeResult) : Math.ceil(tradeResult),
			tradeResultDec = Math.abs(tradeResult - tradeResultInt).toFixed(2).replace(/^[0]+/g, '');
		switch (true) {
			case (tradeResult !== null):
				callText = '<span class="nobold">' + ((tradeResult > 0) ? 'YOU WON' : 'TRADE CLOSED') + '</span><div class="eeValDiv"><span class="eeValSpan">' + ((tradeResult < 0) ? '-' : '') + '&pound;</span><span class="eeValSpan">' + Math.abs(tradeResultInt) + '</span><span class="eeValSpan"><span>' + tradeResultDec + '</span></div>';
				break;
			case !this.props.selectedOrderId:
				callText = 'PLACE YOUR TRADE';
				break;
			case (!!eePayout):
				callText = '<span class="nobold">early exit:</span><div class="eeValDiv"><span class="eeValSpan">' + ((eePayout < 0) ? '-' : '') + '&pound;</span><span class="eeValSpan">' + Math.abs(eePayoutInt) + '</span><span class="eeValSpan"><span>' + eePayoutDec + '</span><span>net</span></div>';
				break;
		}
		return (
			<div className="columnParent mainbtn">

				<div className={userBalanceOk && this.props.online ? 'bigbtn up' : 'bigbtn disabled'} data-value={InitConfig.CALL_FLAG} onClick={this.createTrade}>
					<span className={(this.props.orderType === InitConfig.CALL_FLAG) && !this.props.tradeResult ? 'blink' : ''}>call (up)</span>
					<span className="fatarrow"></span>
				</div>
				<div className="bigRndBtnPanel">
					<div className="bigRndBtn">
						<div>
							<section className="circle" id={'radialProgress' + this.props.symbol}></section>
							<div className="clockface">
								<b></b><b></b><b></b><b></b><b></b><b></b><b></b><b></b><b></b><b></b><b></b><b></b>

								<div className={bigButtonClass} onClick={this.requestEarlyExit}>
									<div>
										<h6 className={tradeResult || fiveSecsLeft ? 'blink' : ''} dangerouslySetInnerHTML={{__html: callText}}/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className={userBalanceOk && this.props.online ? 'bigbtn down' : 'bigbtn disabled'} data-value={InitConfig.PUT_FLAG} onClick={this.createTrade}>
					<span className={(this.props.orderType === InitConfig.PUT_FLAG) && !this.props.tradeResult ? 'blink' : ''}>put (down)</span>
					<span className="fatarrow"></span>
				</div>
			</div>);
	}

	addSymbol = (obj) => {
		obj.symbol = this.props.symbol;
		return obj;
	};

	initTimer = (maxValue) => {
		var timerConf = JSON.parse(JSON.stringify(CIRCLE_TIMER_CONF));
		timerConf.id = 'radialProgress' + this.props.symbol;
		timerConf.maxValue = maxValue ? maxValue : this.props.selectedExpiry;
		timerConf.value = 1;
		this.circleTimer = Circles.create(timerConf);
	};

	updateTimer = (value, eePayout) => {
		if (!this.circleTimer) return;
		var circleColor = (eePayout < 0) ? 'rgba(221, 39, 39, 0.8)' : 'rgba(104, 171, 34, 0.8)',
			selectedExpiry = this.props.selectedOrderExpiry || this.props.selectedExpiry;
		this.circleTimer.update(selectedExpiry - value);
		this.circleTimer.updateColors(['transparent', circleColor]);
	};

	createTrade = (e) => {
		var user = this.props.user,
			amount = parseFloat(this.props.amount);
		if (typeof user.username === 'undefined' || user.userbalance < amount || !this.props.online || this.waitasec) return;
		var flags = parseInt(e.currentTarget.getAttribute('data-value'));
		Actions.createTrade(this.addSymbol({
			flags: flags,
			expiration_in: parseInt(this.props.selectedExpiry),
			amount: amount
		}));
		this.waitasec = true;
		window.setTimeout(() => {
			this.waitasec = false;
		}, 1000);
	};

	//noinspection FunctionWithMoreThanThreeNegationsJS
	requestEarlyExit = () => {
		if (!this.props.selectedOrderId || !this.props.user.username || this.props.expiry < 1 || !this.props.earlyExitPayout || !this.props.online) return;
		Actions.requestEarlyExit(this.props.selectedOrderId);
	};
}
