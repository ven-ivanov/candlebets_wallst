'use strict';

import Store from '../stores/chartpanelstore';
import Actions from '../actions/actions';
import RadioGroup from './radiogroup.jsx';
import Chart from './chart.jsx';
import chartPanelConfig from '../utils/chartPanelConfig';

export default class ChartPanel extends React.Component {
	static displayName = 'ChartPanel';

	static propTypes = {
		alias: React.PropTypes.string,
		changeAction: React.PropTypes.string,
		radioGroupsConfig: React.PropTypes.array,
		shift: React.PropTypes.number.isRequired,
		symbol: React.PropTypes.string.isRequired
	};

	static defaultProps = chartPanelConfig;

	constructor(props) {
		super(props);
		this.state = {
			chartres: 'Tick',
			charttype: 'candlestick'
		};
		if (!this.props.symbol) throw new Error('Symbol undefined');
		this.symbol = this.props.symbol;
	}

	componentDidMount() {
		Store.listen(this.onStateChange);
	}

	componentWillUnmount() {
		Store.unlisten(this.onData);
	}

	shouldComponentUpdate(nextProps, nextState) {
		return (nextState.chartres !== this.state.chartres || nextState.charttype !== this.state.charttype);
	}

	render() {
		var chartResRadioGroup = this.props.radioGroupsConfig[0],
			chartTypeRadioGroup = this.props.radioGroupsConfig[1],
			chartResRadioButtons = chartResRadioGroup.buttons[this.state.charttype],
			chartTypeRadioButtons = chartTypeRadioGroup.buttons;
		return (
			/*<div className="flexChild columnParent panel chartPanel">
				<div className="btnbar rowParent">
					<RadioGroup symbol={this.props.symbol} name={chartResRadioGroup.name} buttons={chartResRadioButtons} checkedButton={this.state.chartres} onChangeAction={this.props.changeAction}/>

					<div className="alignright" style={{border: 'none'}}>
						<ul className="radiogroup flright">
							{this.state.charttype === 'candlestick' ? <li>
								<label className="iconFx" onClick={this.openModal}></label>
							</li> : null }
							<li>
								<label className="tradingStation"></label>
							</li>
						</ul>
						<RadioGroup symbol={this.props.symbol} name={chartTypeRadioGroup.name} buttons={chartTypeRadioButtons} checkedButton={this.state.charttype} onChangeAction={this.props.changeAction}/>
					</div>
				</div>
				<Chart charttype={this.state.charttype} chartres={this.state.chartres} {...this.props} />
			</div>*/
            <div>
                <div className="top-bar">
                    <div className="buttons">
                        <div className="btn-group" role="group" aria-label="...">
                            <button type="button" className="btn btn-default">Tick</button>
                            <button type="button" className="btn btn-default">M1</button>
                            <button type="button" className="btn btn-default">M5</button>
                            <button type="button" className="btn btn-default">M15</button>
                        </div>
                        <div className="btn-group" role="group" aria-label="...">
                            <button type="button" className="btn btn-default"><i className="icn-cursor"></i></button>
                            <button type="button" className="btn btn-default"><i className="icn-view"></i></button>
                        </div>
                        <div className="btn-group" role="group" aria-label="...">
                            <button type="button" className="btn btn-default"><i className="icn-plus"></i></button>
                            <button type="button" className="btn btn-default"><i className="icn-minus"></i></button>
                        </div>
                        <div className="btn-group" role="group" aria-label="...">
                            <button type="button" className="btn btn-default"><i className="icn-fx"></i></button>
                        </div>
                        <div className="btn-group" role="group" aria-label="...">
                            <button type="button" className="btn btn-default"><i className="icn-eq"></i></button>
                            <button type="button" className="btn btn-default"><i className="icn-sl"></i></button>
                        </div>
                    </div>
                </div>
                <Chart charttype={this.state.charttype} chartres={this.state.chartres} {...this.props} />
            </div>

		);
	}

	onStateChange = () => {
		this.setState(Store.getState().channels[this.symbol]);
	};

	openModal = () => {
		if (this.state.charttype !== 'candlestick') return;
		Actions.openModal(this.symbol);
	};
}
