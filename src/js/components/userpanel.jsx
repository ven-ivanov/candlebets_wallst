'use strict';

import Actions from '../actions/actions';
import Store from '../stores/userstore';
import fmtMoney from '../utils/fmtmoney';

export default class UserPanel extends React.Component {
	static displayName = 'UserPanel';

	constructor(props) {
		super(props);
		this.state = {
			userbalance: 0,
			username: null
		};
	}

	componentDidMount() {
		Store.listen(this.onStateChange);
	}

	componentWillUnmount() {
		Store.unlisten(this.onStateChange);
	}

	render() {
		var content = this.state.username ? <span>
			<dt>Balance: <span
				className={this.state.userbalance > 0 ? 'up' : 'down'}>{fmtMoney(this.state.userbalance)}</span></dt>
			<dt style={{padding: '2px 3px 2px 9px'}}>
				{this.state.username}
				<button className="btn redhover" onClick={()=>{Actions.userLogout();}}>Logout</button>
			</dt>
			</span> :
			<div>
				<form onSubmit={this.userLogin}>
					<input type="text" name="username" placeholder="Login" maxLength="30"/>
					<input type="password" name="password" placeholder="Password" maxLength="30"/>
					<button className="btn" type="submit">Login</button>
				</form>
			</div>;
		return (
			<div className="panel userPanel">
				<div className="btnbar">
					{content}
				</div>
			</div>
		);
	}

	onStateChange = () => {
		this.setState(Store.getState());
	};

	userLogin = (e) => {
		e.preventDefault();
		var form = e.target,
			auth = form.username.value,
			password = form.password.value;
		if (auth && password) Actions.userLogin({auth, password});
	};
}
