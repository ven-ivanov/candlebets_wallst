'use strict';

import Actions from '../actions/actions';

export default class ExpirySlider extends React.Component {
	static displayName = 'ExpirySlider';

	static propTypes = {
		label: React.PropTypes.string,
		ticks: React.PropTypes.array.isRequired,
		symbol: React.PropTypes.string.isRequired
	};

	static defaultProps = {
		label: 'Seconds to Expiry',
		ticks: ['15', '30', '60', '120', '180', '300', '600', '900']
	};

	constructor(props) {
		super(props);
		this.state = {expiryValue: 15};
	}

	componentDidMount() {
		var defaultTick = React.findDOMNode(this.refs.sliderpath).querySelector('span[data-value="' + this.state.expiryValue + '"]');
		this.positionSliderThumb(defaultTick);
	}

	shouldComponentUpdate(nextProps, nextState) {
		return (nextState.expiryValue !== this.state.expiryValue);
	}

	render() {
		var sliderContent = this.props.ticks.map((m, i)=> {
			return (
				<span className={this.state.expiryValue == m ? 'active' : ''} data-value={m} key={i} onClick={this.setValue}>
                {m + '\nsec'}
                </span>
			);
		});
		return (<div className="sliderdiv">
				<label>{this.props.label}:</label>

				<div className="sliderpath" ref="sliderpath">
					{sliderContent}
					<div className="sliderthumb" ref="sliderthumb">
						<div></div>
					</div>
				</div>
			</div>
		);
	}

	addSymbol = (obj) => {
		obj.symbol = this.props.symbol;
		return obj;
	};

	positionSliderThumb = () => {
		var sliderPath = React.findDOMNode(this.refs.sliderpath),
			sliderThumb = React.findDOMNode(this.refs.sliderthumb),
			currentTick = sliderPath.querySelector('span[data-value="' + this.state.expiryValue + '"]'),
			sliderThumbPosition = Math.round((currentTick.getBoundingClientRect().left - sliderPath.getBoundingClientRect().left) - 6);
		sliderThumb.style.left = sliderThumbPosition + 'px';
	};

	setValue = (e) => {
		var val = e.currentTarget.getAttribute('data-value');
		Actions.setExpiryValue(this.addSymbol({value: val}));
		this.setState({expiryValue: val}, () => this.positionSliderThumb());
	};
}
