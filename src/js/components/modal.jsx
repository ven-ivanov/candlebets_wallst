'use strict';

import Store from '../stores/modalstore';
import addons from 'react/addons';

const ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

export default class Modal extends React.Component {
	static displayName = 'Modal';

	static propTypes = {
		height: React.PropTypes.string,
		width: React.PropTypes.string
	};

	static defaultProps = {
		width: '400px',
		height: '200px'
	};

	constructor(props) {
		super(props);
		this.state = {
			isOpen: false
		}
	}

	componentDidMount() {
		Store.listen(this.onStateChange);
	}

	componentWillUnmount() {
		Store.unlisten(this.onStateChange);
	}

	render() {
		if (this.state.isOpen) {
			return (
				<ReactCSSTransitionGroup transitionName={this.props.transitionName}>
					<div className="overlay">
						<div className="modal" style={{width: this.props.width, height: this.props.height}}>
							{this.props.children}
						</div>
					</div>
				</ReactCSSTransitionGroup>
			);
		} else {
			return <ReactCSSTransitionGroup transitionName={this.props.transitionName}/>;
		}
	}

	onStateChange = () => {
		this.setState(Store.getState());
	};
}
