'use strict';

import Actions from '../actions/actions';
import Ticker from '../components/ticker.jsx';
import ExpirySlider from '../components/expiryslider.jsx';
import AmountInput from '../components/amountinput.jsx';
import ChartPanel from '../components/chartpanel.jsx';
import TradesTable from '../components/tradestable.jsx';
import TradeControl from '../components/tradecontrol.jsx';

export default class MainSection extends React.Component {
	static displayName = 'MainSection';

	static propTypes = {
		alias: React.PropTypes.string,
		online: React.PropTypes.bool,
		shift: React.PropTypes.number.isRequired,
		symbol: React.PropTypes.string.isRequired
	};

	constructor(props) {
		super(props);
		if (!this.props.symbol) throw new Error('Symbol undefined');
		this.symbol = this.props.symbol;
	}

	componentDidMount() {
		Actions.subscribeToQuoteFeed(this.symbol);
	}

	componentWillUnmount() {
		Actions.subscribeToQuoteFeed(this.symbol, true);
	}

	render() {
		var assetLabelArr = this.props.alias.split('/'),
			upLabel = assetLabelArr[0],
			downLabel = assetLabelArr[1],
			assetInfoContent;
		assetInfoContent = '<span class="down">' + upLabel + '</span> / <span class="up">' + downLabel + '</span>';
        var cx = React.addons.classSet;
        var classes = cx({'mainSection': true,
        'tab-pane': true,
        'fade': true,
        'active': this.props.active,
        'in': this.props.active});
		return (
            //active in
            <section className={classes} id={this.props.tab}>
                <ChartPanel {...this.props} />
            </section>
		);
	}
}
