'use strict';

import SettingsStore from '../stores/tradecontrolstore';
import SelectedTradeStore from '../stores/tradetablestore';
import UserStore from '../stores/userstore';
import BigButtonPanel from './bigbuttonpanel.jsx';
import InfoPanel from './infopanel.jsx';
import InitConfig from '../utils/initconfig';
import gimmeNum from '../utils/gimmenum';

export default class TradeControl extends React.Component {
	static displayName = 'TradeControl';

	static propTypes = {
		online: React.PropTypes.bool,
		symbol: React.PropTypes.string.isRequired
	};

	constructor(props) {
		super(props);
		if (!this.props.symbol) throw new Error('Symbol undefined');
		this.state = {
			roi: InitConfig.ROI,
			amount: InitConfig.INIT_AMOUNT,
			selectedAmount: InitConfig.INIT_AMOUNT,
			expiry: InitConfig.INIT_EXPIRY,
			selectedExpiry: InitConfig.INIT_EXPIRY,
			selectedOrderId: null,
			selectedOrderExpiry: InitConfig.INIT_EXPIRY,
			orderType: null,
			earlyExitPayout: 0,
			tradeResult: null,
			user: {}
		};
	}

	componentDidMount() {
		SettingsStore.listen(this.updateSettings);
		SelectedTradeStore.listen(this.updateTrade);
		UserStore.listen(this.updateUser);
	}

	componentWillUnmount() {
		SettingsStore.unlisten(this.updateSettings);
		SelectedTradeStore.unlisten(this.updateTrade);
		UserStore.unlisten(this.updateUser);
	}

	render() {
		return (
			<div className="flexChild rowParent">
				<BigButtonPanel symbol={this.props.symbol} user={this.state.user} online={this.props.online} amount={this.state.amount} selectedExpiry={this.state.selectedExpiry} expiry={this.state.expiry} selectedOrderId={this.state.selectedOrderId} orderType={this.state.orderType} earlyExitPayout={this.state.earlyExitPayout} selectedOrderExpiry={this.state.selectedOrderExpiry} tradeResult={this.state.tradeResult}/>
				<InfoPanel symbol={this.props.symbol} roi={this.state.roi} amount={this.state.amount} selectedAmount={this.state.selectedAmount} expiry={this.state.expiry} selectedExpiry={this.state.selectedExpiry} selectedOrderId={this.state.selectedOrderId} tradeResult={this.state.tradeResult}/>
			</div>
		);
	}

	updateSettings = () => {
		if (this.state.selectedOrderId === null) this.setState(SettingsStore.getState().channels[this.props.symbol]);
	};

	updateUser = () => {
		var user = UserStore.getState();
		this.setState({user: {username: user.username, userbalance: user.userbalance}});
	};

	updateTrade = () => {
		var channel = SelectedTradeStore.getState().channels[this.props.symbol],
			selectedTradeIndex = channel.selectedTradeIndex;

		if (channel.tradeResult && !this.state.tradeResult) {
			this.setState({tradeResult: channel.tradeResult});
			return;
		}

		if (this.state.tradeResult && !channel.tradeResult) {
			this.setState({tradeResult: null});
		}

		if (selectedTradeIndex !== null) {
			var selectedOrder = channel.openTrades[selectedTradeIndex];
			this.setState({
				amount: gimmeNum(selectedOrder['bet amount']),
				expiry: selectedOrder.time,
				earlyExitPayout: gimmeNum(selectedOrder['profit/loss']),
				selectedOrderId: selectedOrder.ticket,
				selectedOrderExpiry: selectedOrder.expiry,
				orderType: selectedOrder.flags
			});
			this.reset = true;
			return;
		}
		if (this.reset) {
			this.setState(SettingsStore.getState().channels[this.props.symbol]);
			this.reset = false;
		}
	};
}
