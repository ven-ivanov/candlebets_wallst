'use strict';

import Actions from '../actions/actions';

export default class AmountInput extends React.Component {
	static displayName = 'AmountInput';

	static propTypes = {
		defaultValue: React.PropTypes.number,
		label: React.PropTypes.string,
		maxValue: React.PropTypes.number,
		minValue: React.PropTypes.number,
		online: React.PropTypes.bool,
		symbol: React.PropTypes.string.isRequired
	};

	static defaultProps = {
		label: 'Amount',
		defaultValue: 100,
		minValue: 1,
		maxValue: 10000,
		online: true
	};

	constructor(props) {
		super(props);
		this.state = {
			inputValue: this.props.defaultValue,
			subscribed: true
		};
	}

	render() {
		var ledClassName = this.state.subscribed && this.props.online ? 'led active' : 'led';

		return (
			<div className="amountdiv">
				<div className="inputgroup">
					<label>{this.props.label} &pound;</label>
					<select className="alignright" defaultValue={this.props.defaultValue} value={this.state.inputValue}
					        onChange={this.handleChange}>
						<option value="5">5</option>
						<option value="10">10</option>
						<option value="25">25</option>
						<option value="50">50</option>
						<option value="100">100</option>
						<option value="250">250</option>
						<option value="500">500</option>
						<option value="1000">1000</option>
						<option value="2500">2500</option>
						<option value="other">other</option>
					</select>
					<input className="amount" type="number" min={this.props.minValue} max={this.props.maxValue}
					       ref="amountInput" onChange={this.handleChange}/>
				</div>
				<span className={ledClassName}></span>
			</div>
		);
	}

	addSymbol = (obj) => {
		obj.symbol = this.props.symbol;
		return obj;
	};

	handleChange = (e) => {
		var control = e.currentTarget,
			val = control.value,
			amountInput = React.findDOMNode(this.refs.amountInput);
		if (control.tagName.toLowerCase() === 'select') {
			amountInput.value = '';
			amountInput.style.display = (!/^\d{0,4}$/.test(val)) ? 'inline-block' : 'none';
		}

		if (!/^\d{0,4}$/.test(val) || parseInt(val) < this.props.minValue || parseInt(val) > this.props.maxValue) {
			val = 0;
		}
		Actions.setAmountValue(this.addSymbol({value: val}));
		this.setState({inputValue: val});
	};

	/* todo: remove in production
	 // <span className={ledClassName} onClick={this.subUnsub}></span>
	 subUnsub = () => {
	 if (!this.props.online) return;
	 this.setState({subscribed: !this.state.subscribed}, ()=> {
	 Actions.subscribeToQuoteFeed(this.props.symbol, !this.state.subscribed);
	 });
	 };
	 */
}
