'use strict';

import Actions from '../actions/actions';

export default class IndicatorsForm extends React.Component {
	static displayName = 'IndicatorsForm';

	static propTypes = {
		symbol: React.PropTypes.string
	};

	constructor(props) {
		super(props);
		this.state = {
			symbol: this.props.symbol,
			indicator: 'MA',
			type: 'simple',
			base: 'close',
			period: '14'
		};
		if (!this.props.symbol) throw new Error('Symbol undefined');
	}

	render() {
		return (
			<form className="body" onSubmit={this.addIndicator}>
				<div className="formrow">
					<div>
						<label>Indicator:</label>
						<select name="indicator" value={this.state.indicator} onChange={this.onIndicatorChange}>
							<option value="MA">MA</option>
							<option value="BB">BB</option>
						</select>
					</div>
					<div>
						<label>Base:</label>
						<select name="base" value={this.state.base} onChange={this.handleChange}>
							<option value="open">Open</option>
							<option value="high">High</option>
							<option value="low">Low</option>
							<option value="close">Close</option>
						</select>
					</div>
				</div>
				<div className="formrow">
					{this.state.indicator === 'MA' ? <div>
						<label>Type:</label>
						<select name="type" value={this.state.type} onChange={this.handleChange}>
							<option value="simple">Simple</option>
							<option value="exponential">Exponential</option>
							<option value="weighted">Weighted</option>
							<option value="cumulative">Cumulative</option>
						</select>
					</div> : null }
					<div>
						<label>Period:</label>
						<input name="period" type="number" min="1" max="20" value={this.state.period} onChange={this.handleChange}/>
					</div>
				</div>
				<input type="hidden" name="symbol" value={this.props.symbol}/>

				<div className="formbuttons">
					<button type="submit" onClick={this.addIndicator}>Add / View</button>
					<button type="cancel" onClick={this.closeModal}>Clear / Cancel</button>
				</div>
			</form>
		);
	}

	onIndicatorChange = (e) => {
		this.setState({indicator: e.target.value});
	};

	handleChange = (e) => {
		this.setState({[e.target.name]: e.target.value});
	};

	addIndicator = (e) => {
		e.preventDefault();
		Actions.setIndicator(this.state);
	};

	closeModal = (e) => {
		e.preventDefault();
		Actions.setIndicator({symbol: this.props.symbol});
	};
}
