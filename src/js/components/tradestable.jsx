'use strict';

import Store from '../stores/tradetablestore';
import Actions from '../actions/actions';
import Griddle from 'griddle-react';
import OrderControlButtons from './ordercontrolbuttons.jsx';
import DateDisplay from './datedisplay.jsx';

const columnMetaOpenTrades = [
		{columnName: 'profit/loss', customComponent: OrderControlButtons},
		{columnName: 'open time', customComponent: DateDisplay}
	],
	columnMetaClosedTrades = [
		{columnName: 'open time', customComponent: DateDisplay},
		{columnName: 'close time', customComponent: DateDisplay}
	];

export default class TradesTable extends React.Component {
	static displayName = 'TradesTable';

	static propTypes = {
		alias: React.PropTypes.string.isRequired,
		symbol: React.PropTypes.string.isRequired,
		thsclose: React.PropTypes.array,
		thsopen: React.PropTypes.array.isRequired,
		viewButtons: React.PropTypes.array.isRequired
	};

	static defaultProps = {
		viewButtons: [
			{label: 'open trades', value: 'opentrades'},
			{label: 'close trades', value: 'closetrades'}
		],
		thsopen: ['ticket', 'bet amount', 'payout', 'roi', 'type', 'open', 'expiry', 'open time', 'time', 'profit/loss'],
		thsclose: ['ticket', 'bet amount', 'payout', 'roi', 'type', 'open', 'close', 'expiry', 'open time', 'close time', 'EE', 'profit/loss']
	};

	constructor(props) {
		super(props);
		if (!this.props.symbol) throw new Error('Symbol undefined');
		this.state = Store.getState();
		this.symbol = this.props.symbol;
	}

	componentDidMount() {
		Store.listen(this.onStateChange);
	}

	componentWillUnmount() {
		Store.unlisten(this.onStateChange);
	}

	render() {
		var noDataMsg = 'There are no open trades on this chart!',
			viewSelectorContent = this.props.viewButtons.map((m, i)=> {
				//noinspection JSPotentiallyInvalidUsageOfThis
				return (
					<div className={m.value === this.state.activeView ? 'btn p-t-9 active' : 'btn p-t-9'} data-value={m.value} key={this.props.alias + i} onClick={this.changeView}>{m.label + ' ' + this.props.alias}</div>
				);
			});
		return (
			<div className="panel">
				<div className="btnbar navbar">
					{viewSelectorContent}
				</div>
				<div>
					{(this.state.activeView === 'opentrades') ?
						<Griddle key={'openTradesTable' + this.props.alias} results={this.state.openTrades} columns={this.props.thsopen} useGriddleStyles={false} resultsPerPage={5} columnMetadata={columnMetaOpenTrades} noDataMessage={noDataMsg} initialSort="open time" initialSortAscending={false} tableClassName="openTradesTable"/> :
						<Griddle key={'closeTradesTable' + this.props.alias} results={this.state.closeTrades} columns={this.props.thsclose} useGriddleStyles={false} resultsPerPage={5} columnMetadata={columnMetaClosedTrades} noDataMessage={noDataMsg} initialSort="close time" initialSortAscending={false}/>}
				</div>
			</div>
		);
	}

	addSymbol = (obj) => {
		obj.symbol = this.props.symbol;
		return obj;
	};

	onStateChange = () => {
		this.setState(Store.getState().channels[this.symbol]);
	};

	changeView = (e) => {
		Actions.setTradeTableView(this.addSymbol({value: e.target.getAttribute('data-value')}));
	};

}
