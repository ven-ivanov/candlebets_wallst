const chartPanelConfig = {
	changeAction: 'setChartSettings',
	radioGroupsConfig: [
		{
			name: 'chartres',
			buttons: {
				candlestick: [
					{label: 'TICK', value: 'Tick'},
					{label: 'M1', value: 'M1'},
					{label: 'M5', value: 'M5'},
					{label: 'M15', value: 'M15'},
					{label: 'M30', value: 'M30'},
					{label: 'H1', value: 'H1'},
					{label: 'H4', value: 'H4'},
					{label: 'D1', value: 'D1'}
				],
				areaspline: [
					{label: 'TICK', value: 'Tick'},
					{label: '30m', value: 'S30'},
					{label: '1h', value: 'M1'},
					{label: '2h', value: 'M2'},
					{label: '4h', value: 'M4'},
					{label: '8h', value: 'M8'},
					{label: '12h', value: 'M12'}
				]
			}
		},
		{
			name: 'charttype',
			buttons: [
				{label: '', value: 'candlestick', classname: 'iconCandleChart'},
				{label: '', value: 'areaspline', classname: 'iconSplineChart'}
			]
		}
	]
};

export default chartPanelConfig;
