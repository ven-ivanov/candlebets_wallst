'use strict';

export default function getSid() {
	function a() {
		return Math.floor((1 + Math.random()) * 65536).toString(16).substring(1);
	}
	return a() + a() + '-' + a() + '-' + a() + '-' + a() + '-' + a() + a() + a() + '';
}
