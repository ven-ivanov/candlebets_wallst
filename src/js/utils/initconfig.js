const initConfig = {
	ROI: 97.3,
	INIT_AMOUNT: 100,
	INIT_EXPIRY: 15,
	CALL_FLAG: 5,
	PUT_FLAG: 6
}

export default initConfig;
