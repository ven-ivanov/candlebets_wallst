'use strict';

var indicatorClass = function() {
	var series, params, linecolor = '#FFE700';

	this.roundTo4 = function (val) {
		if (!isNaN(val)) {
			var mult = Math.pow(10, 4);
			return (Math.round(val * mult) / mult);
		} else {
			return val;
		}
	}

	this.getSeries = function () {
		var data = [],
			n, l;
		if (!series) return data;
		if (series.cropped) {
			for (n = 0, l = series.cropStart; n < l; n++) {
				data.push({
					x: series.xData[n],
					open: series.yData[n][0],
					high: series.yData[n][1],
					low: series.yData[n][2],
					close: series.yData[n][3]
				});
			}
		}
		if (series.groupedData) {
			for (n = 0, l = series.groupedData.length; n < l; n++) {
				data.push(series.groupedData[n]);
			}
		}

		l = (series.groupedData) ? series.groupedData.length : series.xData.length;
		for (n = 0; n < l; n++) {
			data.push({
				x: series.xData[n],
				open: series.yData[n][0],
				high: series.yData[n][1],
				low: series.yData[n][2],
				close: series.yData[n][3]
			});
		}
		data.sort(function (a, b) { if (a.x > b.x) return 1; else if (a.x < b.x) return -1; else return 0});
		for (n = data.length - 2; n >= 0; n--) {
			if (data[n].x === data[n + 1].x) data.splice(n, 1);
		}
		return data;
	}

	this.getIndicatorData = function (settings, data, newlinecolor) {
		params = settings;
		series = data;
		linecolor = newlinecolor || '#FFE700';
		switch (params.indicator) {
			case 'MA':
				switch (params.type) {
					case 'simple':
						return this.indicatorSimpleMovingAverage(params.period, params.base);
					case 'exponential':
						return this.indicatorExponentialMovingAverage(params.period, params.base);
					case 'cumulative':
						return this.indicatorCumulativeMovingAverage(params.base);
					case 'weighted':
						return this.indicatorWeightedMovingAverage(params.period, params.base);
				}
				break;
			case 'BB':
				return this.indicatorBollingerBands(params.period, params.base);
		}
	};

	this.indicatorSimpleMovingAverage = function (p, b) {
		var period = parseInt(p) || 14,
			base = b || 'close',
			indicator = {
				name: 'SMA (' + period + ')',
				data: [],
				type: 'spline',
				color: linecolor,
				lineWidth: 1
			},
			SMA = 0,
			chartPoints = this.getSeries();
		if (chartPoints.length < period) {
			return [indicator];
		}
		for (var n = 0, l = period; n < l; n++) {
			SMA += chartPoints[n][base];
		}
		SMA = SMA / period;
		for (n = period, l = chartPoints.length; n < l; n++) {
			SMA = SMA + (chartPoints[n][base] / period) - (chartPoints[n - period][base] / period);
			indicator.data.push([chartPoints[n].x, this.roundTo4(SMA)]);
		}
		return [indicator];
	}

	this.indicatorCumulativeMovingAverage = function (b) {
		var base = b || 'close',
			indicator = {
				name: 'CMA',
				data: [],
				type: 'spline',
				color: linecolor,
				lineWidth: 1
			},
			chartPoints = this.getSeries();
		if (!chartPoints.length) {
			return [indicator];
		}
		var CMA = chartPoints[0][base];
		for (var n = 1, l = chartPoints.length; n < l; n++) {
			CMA = CMA + (chartPoints[n][base] - CMA) / n;
			indicator.data.push([chartPoints[n].x, this.roundTo4(CMA)]);
		}
		return [indicator];
	}

	this.indicatorWeightedMovingAverage = function (p, b) {
		var period = parseInt(p) || 14,
			base = b || 'close',
			indicator = {
				name: 'WMA (' + period + ')',
				data: [],
				type: 'spline',
				color: linecolor,
				lineWidth: 1
			},
			points = [],
			mult = 2 / (period * (period + 1)),
			chartPoints = this.getSeries(),
			n, l;
		for (n = 0, l = period - 1; n < l; n++) {
			if (chartPoints[n] == undefined) {
				return [indicator];
			} else {
				points.push(chartPoints[n]);
			}
		}
		for (n = period, l = chartPoints.length; n < l; n++) {
			points.push(chartPoints[n]);
			var _val = 0;
			for (var i = points.length - 1; i >= 0; i--) {
				_val += (period - i) * chartPoints[n - i][base];
			}
			indicator.data.push([chartPoints[n].x, this.roundTo4(mult * _val)]);
			points.shift();
		}
		return [indicator];
	}

	this.indicatorExponentialMovingAverage = function (p, b) {
		var period = parseInt(p) || 14,
			base = b || 'close',
			indicator = {
				name: 'EMA (' + period + ')',
				data: [],
				type: 'spline',
				color: linecolor,
				lineWidth: 1
			},
			alpha = 2 / (period + 1),
			chartPoints = this.getSeries();
		if (!chartPoints.length) {
			return [indicator];
		}
		var EMA = chartPoints[0][base];
		indicator.data.push([chartPoints[0].x, this.roundTo4(EMA)]);
		for (var n = 1, l = chartPoints.length; n < l; n++) {
			EMA = alpha * chartPoints[n][base] + (1 - alpha) * EMA;
			indicator.data.push([chartPoints[n].x, this.roundTo4(EMA)]);
		}
		return [indicator];
	};

	this.indicatorBollingerBands = function (p, b) {
		var period = parseInt(p) || 14,
			base = b || 'close',
			indicator = [
				{
					name: 'BB (' + period + ')',
					data: [],
					type: 'spline',
					color: linecolor,
					lineWidth: 1
				}, {
					name: 'BB High (' + period + ')',
					data: [],
					type: 'spline',
					color: linecolor,
					lineWidth: 1
				}, {
					name: 'BB Low (' + period + ')',
					data: [],
					type: 'spline',
					color: linecolor,
					lineWidth: 1
				}
			],
			SMA = 0,
			last = [],
			chartPoints = this.getSeries();
		if (chartPoints.length < period) {
			return indicator;
		}
		for (var n = 0, l = period; n < l; n++) {
			SMA += chartPoints[n][base];
			last.push(chartPoints[n][base]);
		}
		SMA = SMA / period;
		for (n = period, l = chartPoints.length; n < l; n++) {
			last.shift();
			last.push(chartPoints[n][base]);
			var sigma = 0;
			for (var i = 0; i < period; i++) {
				sigma += Math.pow(last[i] - SMA, 2);
			}
			sigma = Math.sqrt(sigma / period);
			SMA = SMA + (chartPoints[n][base] / period) - (chartPoints[n - period][base] / period);
			indicator[0].data.push([chartPoints[n].x, this.roundTo4(SMA)]);
			indicator[1].data.push([chartPoints[n].x, this.roundTo4(SMA + (2 * sigma))]);
			indicator[2].data.push([chartPoints[n].x, this.roundTo4(SMA - (2 * sigma))]);
		}
		return indicator;
	};
}

export default new indicatorClass();
