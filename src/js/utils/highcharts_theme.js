'use strict';

var theme = {
	chart: {
		backgroundColor: 'transparent',
		animation: false,
		margin: [0, 45, 25, 0],
		style: {
			fontFamily: 'Arial, Helvetica, sans-serif'
		},
		plotBorderColor: '#606063',
		panning: false
	},
	dateFormat: '%H:%M:%S',
	title: {
		text: null
	},
	subtitle: {
		enabled: false,
		style: {
			color: '#E0E0E3',
			textTransform: 'uppercase'
		}
	},
	xAxis: {
		type: 'datetime',
		tickPixelInterval: 150,
		gridLineWidth: 2,
		gridLineColor: '#262626',
		minorGridLineColor: '#262626',
		minorGridLineWidth: 0,
		minorTickLength: 5,
		minorTickWidth: 1,
		minorTickInterval: 'auto',
		minorTickColor: '#262626',
		labels: {
			style: {
				color: '#9E9EA0',
				fontSize: '8px',
				fontWeight: 'bold'
			}
		},
		lineColor: '#262626',
		tickColor: '#262626',
		title: {
			text: null,
			style: {
				color: '#5b5b5b'
			}
		}
	},
	yAxis: {
		gridLineColor: '#262626',
		labels: {
			x: 25,
			y: 0,
			style: {
				color: '#ffffff',
				fontSize: '8px',
				fontWeight: 'bold'
			}
		},
		lineColor: '#262626',
		minorGridLineColor: '#262626',
		tickColor: '#262626',
		tickWidth: 1,
		minorTickInterval: 'auto',
		title: {
			text: null
		},
		opposite: true,
		plotLines: [
			{
				width: 1,
				zIndex: 6,
				label: {
					align: 'right',
					x: 40,
					y: 2,
					useHTML: true,
					style: {
						color: '#fff',
						fontSize: '8px',
						fontWeight: 'bold',
						padding: '3px 3px 3px 20px'
					}
				}
			}
		]
	},
	tooltip: {
		animation: false,
		xDateFormat: '%H:%M:%S',
		valueDecimals: 4,
		backgroundColor: 'rgba(0, 0, 0, 0.5)',
		shadow: false,
		borderWidth: 0,
		style: {
			color: '#F0F0F0',
			fontSize: '10px',
			padding: '2px 3px'
		},
		headerFormat: '<small>{point.key}</small>',
		pointFormat: '<span style="color:red; margin: auto 2px"> \u25B6 </span> {point.y}'
	},

	plotOptions: {
		series: {
			animation: false,
			dataLabels: {
				color: '#B0B0B3'
			},
			marker: {
				enabled: false,
				lineColor: '#333'
			},
			dataGrouping: {
				enabled: false,
				dateTimeLabelFormats: {
					minute: ['%b %e, %H:%M', '%b %e, %H:%M', '-%H:%M']
				}
			}
		},
		boxplot: {
			fillColor: '#505053'
		},
		candlestick: {
			color: 'red',
			lineColor: 'red',
			upColor: 'green',
			upLineColor: 'green',
			cropThreshold: 40,
			tooltip: {
				xDateFormat: '%H:%M:%S',
				pointFormat: '<span style="color:red; margin: auto 2px"> \u25B6 </span> <b>O</b> {point.open} | <b>H</b> {point.high} | <b>L</b> {point.low} | <b>C</b> {point.close}'
			},
			pointWidth: 3
		},
		errorbar: {
			color: 'white'
		},
		areaspline: {
			color: '#089400',
			fillColor: {
				linearGradient: {
					x1: 0,
					y1: 0,
					x2: 0,
					y2: 1
				},
				stops: [
					[0, 'rgba(8, 148, 0, 1)'], [1, 'rgba(8, 148, 0, 0.25)']
				]
			},
			marker: {
				radius: 2
			},
			lineWidth: 1,
			states: {
				hover: {
					lineWidth: 1
				}
			},
			threshold: null,
			cropThreshold: 40
		}
	},

	legend: {
		enabled: false
	},
	exporting: {
		enabled: false
	},
	credits: {
		enabled: false
	},

	labels: {
		style: {
			color: '#707073'
		}
	},

	scrollbar: {
		enabled: false
	},

	rangeSelector: {
		enabled: false
	},

	navigator: {
		enabled: false,
		height: 30,
		margin: 0
	}
};

export default theme;
