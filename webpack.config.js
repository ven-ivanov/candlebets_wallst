var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
	target: 'web',
	cache: true,
	debug: true,
	devtool: 'eval',
	entry: ['./src/style/main_src.css', './src/js/bootstrap.jsx'],
	output: {
		path: './assets/js',
		filename: 'bundle.min.js',
		pathinfo: true
	},
	resolve: {
		extensions: ['', '.js', '.jsx']
	},
	noParse: [/utils/],
	module: {
		loaders: [
			/*{
				test: /\.(js|jsx)$/,
				exclude: [/node_modules/, /assets/, /vendor/],
				loader: 'jsx-loader?insertPragma=React.DOM&harmony'
			},*/
			{
				test: /\.jsx?$/,
				exclude: [/node_modules/, /assets/, /vendor/],
				loader: 'babel',
				query: {
					optional: ['runtime'],
					stage: 0
				}
			},
			{
				test: /\.css$/,
				exclude: /node_modules/,
				loader: ExtractTextPlugin.extract('style-loader', 'css-loader!cssnext-loader')
			},
			{
				test: /\.png$/,
				loader: 'url-loader?limit=15000&mimetype=image/png&name=../img/[name].[ext]'
			},
			/*{ test: /\.(otf|eot|svg|ttf|woff)$/, loader: 'url-loader?limit=8192'},*/
			{
				test: /\.jpg$/,
				loader: 'file-loader?name=../img/[name].[ext]'
			}
		]
	},
	//postcss: [autoprefixer, csswring, cssnext],
	plugins: [
		new ExtractTextPlugin('../css/main.css', {allChunks: true})
	],
	externals: {
		'react': 'React'
	}
};
