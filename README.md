Developer workflow:

1. Install node on your local machine: https://nodejs.org/download/

2. Unpack the CandleBets.zip

3. Open command prompt and navigate to the 'CandleBets' directory

4. Type 'npm install' command and wait for the NPM to install required packages

5. Edit CSS / JS / JSX files in the 'candlebets/src' folders as necessary. For example, to change the socket server address, edit lines 3 and 4 in the 'candlebets/src/utils/server.js' file.

6. To rebuild the bundle script package when developing, type 'npm run dev' in the command prompt window. This will automatically compile changes to the source code into updated 'candlebets/assets/js/bundle.min.js'.

6. To generate production version of the 'bundle.min.js' app script, type 'npm run prod' in the command prompt window. Wait for the dev tools to rebuild the app bundle.

7. Upload updated 'candlebets/assets/js/bundle.min.js' and 'candlebets/assets/css/main.css' to your prodution server.

NB: The production minification process may mangle CSS style of the big round buttons text. To fix, please do the following:

1. Open the 'assets/css/main.css' file.
2. Replace the

@font-face{font-family:arialblack;font-weight:900}

string with the

@font-face{font-family:'arialblack';src:local('Arial Black');font-weight:900;}